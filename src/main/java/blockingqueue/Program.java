package blockingqueue;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

public class Program  {

  public static void main(String[] args) {
    BlockingQueue<Integer> blockingQueue = new LinkedBlockingDeque<>();

    BlockingQueuePut blockingQueuePut = new BlockingQueuePut(blockingQueue, 5);
    BlockingQueueGet blockingQueueGet = new BlockingQueueGet(blockingQueue, 5);

    blockingQueuePut.start();
    blockingQueueGet.start();
  }
}
