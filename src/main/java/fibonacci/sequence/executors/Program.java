package fibonacci.sequence.executors;

public class Program {

  public static void main(String[] args) {
    FibonacciSequence fibonacciSequence = new FibonacciSequence(15);
    fibonacciSequence.executor();
  }
}
