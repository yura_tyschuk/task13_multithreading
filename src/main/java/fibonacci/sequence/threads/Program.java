package fibonacci.sequence.threads;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Program {

  private static Logger logger = LogManager.getLogger();
  public static void main(String[] args) {
    FibonacciNumbers fibonacciNumbers = new FibonacciNumbers(10, 1000);
    Thread fibonacciSequence = new Thread(fibonacciNumbers, "Fib numbers");
    fibonacciSequence.start();
    FibonacciNumbers fibonacciNumbers1 = new FibonacciNumbers(15, 2000);
    Thread fibonacciSequence1 = new Thread(fibonacciNumbers1, "Fib numbers1");
    fibonacciSequence1.start();
    logger.warn(fibonacciSequence.getName() + " started");
    logger.warn(fibonacciSequence1.getName() + " started");
  }
}
