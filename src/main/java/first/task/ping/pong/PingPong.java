package first.task.ping.pong;

import java.time.LocalDateTime;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


class PingPong {

  private volatile static long tmp;
  private static Object sync;
  private Thread t1;
  private Thread t2;
  private static Logger logger = LogManager.getLogger();

  PingPong() {
    tmp = 0;
    sync = new Object();

  }

  void show() {

    startThread1();
    startThread2();

    t1.start();
    t2.start();
    try {
      t1.join();
      t2.join();

    } catch (InterruptedException e) {
      logger.warn(LocalDateTime.now());
      logger.warn("variable = " + tmp);
    }
  }

  private void startThread1() {
    t1 = new Thread(() -> {
      synchronized (sync) {
        for (int i = 1; i <= 200000; i++) {
          try {
            sync.wait();
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          tmp++;
          sync.notify();
        }
        logger.warn("finish " + Thread.currentThread().getName());
      }
    });
  }

  private void startThread2() {
    t2 = new Thread(() -> {
      synchronized (sync) {
        for (int i = 1; i <= 200000; i++) {
          sync.notify();
          try {
            sync.wait();
          } catch (InterruptedException e) {
          }
          tmp++;
        }
        System.out.println("finish " + Thread.currentThread().getName());
      }
    });


  }
}