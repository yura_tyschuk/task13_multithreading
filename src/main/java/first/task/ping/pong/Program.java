package first.task.ping.pong;

import java.time.LocalDateTime;

public class Program {

  public static void main(String[] args) {
    PingPong pingPong = new PingPong();
    System.out.println(LocalDateTime.now());
    pingPong.show();
    System.out.println(LocalDateTime.now());
  }
}
