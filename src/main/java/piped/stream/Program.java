package piped.stream;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class Program {

  public static void main(String[] args) {
    try {
      PipedOutputStream pout1 = new PipedOutputStream();
      PipedInputStream pin1 = new PipedInputStream(pout1);

      WriteClass writeClass = new WriteClass(pout1);
      ReadClass readClass = new ReadClass(pin1);

      writeClass.start();
      readClass.start();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
