package readwrite.lock;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

class Program {

  private static Logger logger = LogManager.getLogger();

  public static void main(String[] args) {
    ReadWriteLock lock = new ReadWriteLock();
    ExecutorService service = Executors.newFixedThreadPool(4);

    for (int i = 0; i < 4; i++) {
      service.submit(new Thread(new Student((i + 1), lock)));
    }

    try {
      TimeUnit.SECONDS.sleep(2);
      service.shutdownNow();
      logger.warn("Process is stopped");

    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
    }

  }
}