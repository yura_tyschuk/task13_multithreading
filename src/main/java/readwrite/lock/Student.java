package readwrite.lock;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

class Student implements Runnable {

  private static final double WRITE_PROB = 0.5;
  private static final Random rand = new Random();
  private final ReadWriteLock theLock;
  private final int course;
  private static Logger logger = LogManager.getLogger();

  public Student(int course, ReadWriteLock lock) {
    this.course = course;
    theLock = lock;
  }

  public void run() {
    logger.warn("Student: " + course + " Started thread");
    while (!Thread.currentThread().isInterrupted()) {
      double r = rand.nextDouble();
      if (r <= WRITE_PROB) {
        write();
      } else {
        read();
      }
    }
  }

  private void read() {
    try {
      theLock.readLock();
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
    }
    try {
      TimeUnit.MILLISECONDS.sleep(2);
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
    } finally {
      theLock.readUnlock();
    }
    logger.warn("Student: " + course + " Finished reading.");

  }

  private void write() {
    try {
      theLock.writeLock();
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
    }
    try {
      TimeUnit.MILLISECONDS.sleep(2);
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
    } finally {
      theLock.writeUnlock();
    }
    logger.warn("Student: " + course + " Finished writing.");
  }

}