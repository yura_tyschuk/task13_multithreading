package synchronize;

public class Program {

  public static void main(String[] args) {
    TestObj obj = new TestObj();
    TestObj obj1 = new TestObj();
    TestObj obj2 = new TestObj();

    TestPrint testPrint1 = new TestPrint(obj);
    TestPrint testPrint2 = new TestPrint(obj);
    TestPrint testPrint3 = new TestPrint(obj);

    testPrint1.start();
    testPrint2.start();
    testPrint3.start();

//    SynchronizedTestPrint synchronizedTestPrint = new SynchronizedTestPrint(obj);
//    SynchronizedTestPrint synchronizedTestPrint1 = new SynchronizedTestPrint(obj1);
//    SynchronizedTestPrint synchronizedTestPrint2 = new SynchronizedTestPrint(obj2);
//
//    synchronizedTestPrint.start();
//    synchronizedTestPrint1.start();
//    synchronizedTestPrint2.start();
  }
}
