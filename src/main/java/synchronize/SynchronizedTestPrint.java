package synchronize;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SynchronizedTestPrint extends Thread {

  private final TestObj testObj;
  private static Logger logger = LogManager.getLogger();

  SynchronizedTestPrint(TestObj testObj) {
    this.testObj = testObj;
  }

  @Override
  public void run() {
    synchronized (testObj) {
      logger.warn(Thread.currentThread().getName());
      testObj.getLine();

    }
  }
}
