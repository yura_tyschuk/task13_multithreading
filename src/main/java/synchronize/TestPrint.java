package synchronize;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TestPrint extends Thread {

  private TestObj testObj;
  private static Logger logger = LogManager.getLogger();

  TestPrint(TestObj testObj) {
    this.testObj = testObj;
  }

  @Override
  public void run() {
    logger.warn(Thread.currentThread().getName());
    testObj.getLine();
  }
}
